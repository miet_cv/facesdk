#coding=utf8

## @file    fixDatabase.py
## @brief   Script for fixing FDB images database.
## @author  Dmitry Matrokhin
## @date    3-November-2015

import os
import re
import sys
import imageConverter

## @brief latinaze i_string by i_dic
## @param i_string in, string to latinize
## @param i_dic in, dicitonary with legend
## @return latinized string
def latinizate(i_string, i_dic):
    for i, j in i_dic.items():
        i_string = i_string.replace(i, j)
    return i_string

# legend for latinizator
legend = {
' ':'_',
',':'',
'а':'a',
'б':'b',
'в':'v',
'г':'g',
'д':'d',
'е':'e',
'ё':'yo',
'ж':'zh',
'з':'z',
'и':'i',
'й':'y',
'к':'k',
'л':'l',
'м':'m',
'н':'n',
'о':'o',
'п':'p',
'р':'r',
'с':'s',
'т':'t',
'у':'u',
'ф':'f',
'х':'h',
'ц':'c',
'ч':'ch',
'ш':'sh',
'щ':'shch',
'ъ':'y',
'ы':'y',
'ь':'',
'э':'e',
'ю':'yu',
'я':'ya',

'А':'a',
'Б':'b',
'В':'v',
'Г':'g',
'Д':'d',
'Е':'e',
'Ё':'yo',
'Ж':'zh',
'З':'z',
'И':'i',
'Й':'y',
'К':'k',
'Л':'l',
'М':'m',
'Н':'n',
'О':'o',
'П':'p',
'Р':'r',
'С':'s',
'Т':'t',
'У':'u',
'Ф':'f',
'Х':'h',
'Ц':'c',
'Ч':'ch',
'Ш':'sh',
'Щ':'shch',
'Ъ':'y',
'Ы':'y',
'Ь':'',
'Э':'e',
'Ю':'yu',
'Я':'ya',
}

## @brief rename folder from russian letters to english by latinize
## @param i_folderName in, name of folder to rename
## @return new folder name
def fixFolderName(i_folderName):
   newFolderName = latinizate(i_folderName, legend)
   os.rename(i_folderName, newFolderName)
   return newFolderName

## @brief fix names of files in folder to 'folderName_0001.jpg'
## @param i_folder in, folder name
def fixFilesInFolder(i_folder):
   os.chdir(i_folder)
   for fullFileName in os.listdir('.'):
      fileName, fileExt = os.path.splitext(fullFileName)
      if fileName.isdigit():
         num = int(fileName)
         newFileName = i_folder + '_{:0>4d}'.format(num) + fileExt
         os.rename(fullFileName, newFileName)
         imageConverter.convertImage(newFileName)
      else:
         os.remove(fullFileName)
   os.chdir('..')


if __name__ == '__main__':
   if len(sys.argv) != 2:
      print('Usage: python fixDatabase.py <path to database>')
   else:
      os.chdir(sys.argv[1])
      for folderName in os.listdir('.'):
         newFolderName = fixFolderName(folderName)
         fixFilesInFolder(newFolderName)
