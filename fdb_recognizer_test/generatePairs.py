## @file    generatePairs.py
## @brief   Script for pairs generating.
## @author  Dmitry Matrokhin
## @date    3-November-2015

import os
import re
import sys
import random
import glob
import math
import itertools

## @brief get all pairs for these arrays
## @param i_source1 in, first source array
## @param i_source1 in, second source array
## @return array of all pairs
def getAllPairsFor2Arrays(i_source1, i_source2):
   result = []
   for p1 in range(len(i_source1)):
      for p2 in range(len(i_source2)):
         result.append((i_source1[p1],i_source2[p2]))
   return result

## @brief get all pairs for this array
## @param i_source in, source array
## @return array of all pairs
def getAllPairs(i_source):
   result = []
   for p1 in range(len(i_source)):
      for p2 in range(p1+1,len(i_source)):
         result.append((i_source[p1],i_source[p2]))
   return result

## @brief generate matches for persons
## @param i_persons in, for these persons we will generate matches
## @param i_pairsCount in, number of pairs we shall generate
## @param i_maxPairsToPerson in, max pairs of images for 1 person
## @return matches array
def generateMatches(i_persons, i_pairsCount, i_maxPairsToPerson):
   minPairsToPerson = math.ceil(i_pairsCount / len(i_persons))
   if i_maxPairsToPerson < minPairsToPerson:
      raise ValueError('argument #4 is too big')
   random.shuffle(i_persons)
   res = []

   for person in i_persons:
      if i_pairsCount == 0:
         break
      imagesCount = len(glob.glob1('./' + person,"*.jpg"))
      imagesPairs = getAllPairs(range(1, imagesCount + 1))

      if minPairsToPerson > len(imagesPairs):
         raise ValueError('We should have more persons for this <setsNum>')

      curPairsCount = min(random.randint(minPairsToPerson, min(len(imagesPairs), i_maxPairsToPerson)), i_pairsCount)
      pairs = random.sample(imagesPairs, curPairsCount)
      for a, b in pairs:
         res.append((person, a, b))

      i_pairsCount -= curPairsCount

   return res

## @brief generate mismatches for persons
## @param i_persons in, for these persons we will generate mismatches
## @param i_pairsCount in, number of pairs we shall generate
## @param i_maxPairsToPerson in, max pairs of images for 1 pair of persons
## @return mismatches array
def generateMismatches(i_persons, i_pairsCount, i_maxPairsToPerson):

   personPairs = getAllPairs(i_persons)
   minPairsToPerson = math.ceil(i_pairsCount / len(personPairs))
   if i_maxPairsToPerson < minPairsToPerson:
      raise ValueError('argument #4 is too big')
   random.shuffle(personPairs)
   res = []
   for pair in personPairs:
      if i_pairsCount == 0:
         break
      imagesCount1 = len(glob.glob1('./' + pair[0],"*.jpg"))
      imagesCount2 = len(glob.glob1('./' + pair[1],"*.jpg"))

      imagesPairs = getAllPairsFor2Arrays(range(1, imagesCount1 + 1), range(1, imagesCount2 + 1))

      if minPairsToPerson > len(imagesPairs):
         raise ValueError('We should have more persons for this <setsNum>')

      curPairsCount = min(random.randint(minPairsToPerson, min(len(imagesPairs), i_maxPairsToPerson)), i_pairsCount)
      pairs = random.sample(imagesPairs, curPairsCount)
      for a, b in pairs:
         res.append((pair[0], a, pair[1], b))

      i_pairsCount -= curPairsCount
   return res

## @brief main function for generating pairs file
def generatePairs():
   filePairs = open(sys.argv[5], 'w')

   os.chdir(sys.argv[1])
   persons = os.listdir('.')
   filePairs.write(sys.argv[2] + ' ' + sys.argv[3] + '\n')
   for setNum in range(int(sys.argv[2])):
      matches = generateMatches(persons, int(sys.argv[3]), int(sys.argv[4]))
      mismatches = generateMismatches(persons, int(sys.argv[3]), int(sys.argv[4]))
      for pair in matches + mismatches:
         for x in pair:
            filePairs.write(str(x) + ' ')
         filePairs.write('\n')


if __name__ == '__main__':
   if len(sys.argv) != 7:
      print('Usage: python generatePairs.py <path> <setsNum> <pairsNum> <maxToOne> <file> <seed>\n'
            '1. path - path to images database\n'
            '2. setsNum - number of sets\n'
            '3. pairsNum - number of matched/mismatched pairs\n'
            '4. maxToOne - maximum number of pairs from one person, and maximum number of pairs from one mans pair\n'
            '5. file - destination file path\n'
            '6. seed - seed for random\n')
   else:
      random.seed(int(sys.argv[6]))
      generatePairs()
