/// @file Tester.h
/// @brief Contains declaration for class CTester.
///        This class was created for testing facesdk library on fdb database
/// @author Dmitry Matrokhin
/// @date Created on: 01-11-2015

#pragma once

#include <string>
#include <map>

#include <pbio/FacerecService.h>

#include "LFW_Dataset.h"
#include "Logger.h"

/// @class CTester
/// @brief Class for testing recognizer by evaluating scores for some pairs of images.
class CTester
{
public:
   /// @brief Constructor
   /// @param i_logFileName in, file with logs
   /// @param i_facerecLibPath in, path to facerec.dll
   /// @param i_facerecConfPath in, path to conf directory
   /// @param i_recognizerConf in, method, that shall be used in recognizer
   CTester(const std::string& i_logFileName
      , const std::string& i_facerecLibPath
      , const std::string& i_facerecConfPath
      , const std::string& i_recognizerConf);

   /// @brief Main function for this class. Allows to evaluate scores for pairs of images
   /// @param i_pairsFilePath in, path to file with pairs
   /// @param i_fdbDir in, path to dir with database of images
   /// @param i_resultScoresFile in, path to results file
   void DoTest(const std::string& i_pairsFilepath
      , const std::string& i_fdbDir
      , const std::string& i_resultScoresFile);

private:
   /// @brief process one folder for some pairs of images.
   /// @param i_dataset in, dataset
   /// @param i_indexes in, vector of pairs of indexes for images
   /// @param i_foldNum in, folder number
   /// @param io_recTimes in/out, time's measures of recognizer work
   /// @param io_scores in/out, scores for pairs of images
   void CTester::processOneFolder(const LFW_Dataset& i_dataset
      , const std::vector<std::pair<int, int> >& i_indexes
      , const int i_foldNum
      , std::vector<double>& io_recTimes
      , std::vector<float>& io_scores);


   const pbio::FacerecService::Ptr m_service;           ///< facerec service
   pbio::Capturer::Ptr m_capturer;                      ///< facerec capturer
   pbio::Recognizer::Ptr m_recognizer;                  ///< facerec recognizer

   std::map<std::string, pbio::Template::Ptr> m_memory; ///< cache for scores of images

   CLogger m_logger;                                    ///< used for logging to logFile.log
};