/// @file Logger.h
/// @brief Contains declaration for class CLogger.
///        This class used for logging to logFile.log file
/// @author Dmitry Matrokhin
/// @date Created on: 01-11-2015

#pragma once

#include <ctime>
#include <string>
#include <chrono>
#include <fstream>

/// @class CLogger
/// @brief Used for logging to logFile.log file
class CLogger
{
public:
   /// @brief Constructor
   /// @param i_logFile in, path to log file
   CLogger(const std::string& i_logFile);

   /// @brief write message to log file
   /// @param i_message in, string to write
   void log(const std::string& i_message);
private:
   std::fstream fout; ///< out stream to log file
};
