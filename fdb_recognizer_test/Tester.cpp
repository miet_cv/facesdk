/// @file Tester.cpp
/// @brief Contains definition for class CTester.
///        This class was created for testing facesdk library on fdb database
/// @author Dmitry Matrokhin
/// @date Created on: 01-11-2015

#include "Tester.h"
#include <iostream>
#include <map>
#include <vector>

#include "common.h"


namespace
{
   /// @brief process one image
   /// @param i_file in, image file
   /// @param i_capturer in, facerec capturer
   /// @param i_recognizer in, facerec recognizer
   /// @param io_memory in/out, cache for scores of images
   pbio::Template::Ptr getTemplate(
      const std::string i_file,
      pbio::Capturer &i_capturer,
      const pbio::Recognizer &i_recognizer,
      std::map<std::string, pbio::Template::Ptr> &io_memory)
   {
      // check if we already processed this file
      std::map<std::string, pbio::Template::Ptr>::iterator iterator = io_memory.find(i_file);
      if (iterator != io_memory.end())
      {
         return iterator->second;
      }

      // loading image file content
      std::vector<uint8_t> file_data;

      std::ifstream sfile(i_file.c_str(), std::ios_base::binary);

      if (!sfile.is_open())
      {
         throw std::runtime_error("can't open file '" + i_file + "'.");
      }

      sfile.seekg(0, sfile.end);
      const size_t len = (size_t)sfile.tellg();

      sfile.seekg(0, sfile.beg);

      file_data.resize(len);

      sfile.read((char*)&file_data[0], len);

      if (!sfile.good())
      {
         throw std::runtime_error("error reading file '" + i_file + "'.");
      }

      // capture samples from image
      const std::vector<pbio::RawSample::Ptr> samples = i_capturer.capture(file_data.data(), file_data.size());
      pbio::Template::Ptr result;

      if (samples.size() == 1)
      {
         result = i_recognizer.processing(*samples[0]);
      }
      else
      {
         throw pbio::Error("0x88 Unable to capture single face");
      }

      // save result for this file
      io_memory[i_file] = result;

      return result;
   }

   /// @brief print result scores to file
   /// @param i_fileName in, name of results file
   /// @param i_matchScores in, match scores
   /// @param i_mismatchScores in, mismatch scores
   void printScoresIntoFile(const std::string& i_fileName
      , const std::vector<float>& i_matchScores
      , const std::vector<float>& i_mismatchScores)
   {
      std::fstream fout(i_fileName, std::ios::out);

      std::copy(i_matchScores.begin(), i_matchScores.end(),
         std::ostream_iterator<float>(fout, " "));
      fout << std::endl;
      std::copy(i_mismatchScores.begin(), i_mismatchScores.end(),
         std::ostream_iterator<float>(fout, " "));

      fout.close();
   }
} // end: unnamed namespace

//////////////////////////////////////////////////////////////////////////
CTester::CTester(const std::string& i_logFileName
   , const std::string& i_facerecLibPath
   , const std::string& i_facerecConfPath
   , const std::string& i_recognizerConf)
   : m_logger(i_logFileName)
   , m_service(pbio::FacerecService::createService(i_facerecLibPath, i_facerecConfPath))
   , m_capturer(m_service->createCapturer("common_capturer4.xml"))
   , m_recognizer(m_service->createRecognizer(i_recognizerConf))
{
   // create capturer
   //m_capturer = m_service->createCapturer("common_capturer4.xml");

   // create recognizer
   //m_recognizer = m_service->createRecognizer(i_recognizerConf);
}

//////////////////////////////////////////////////////////////////////////
void CTester::processOneFolder(const LFW_Dataset& i_dataset
   , const std::vector<std::pair<int, int> >& i_indexes
   , const int i_foldNum
   , std::vector<double>& io_recTimes
   , std::vector<float>& io_scores)
{
   for (size_t j = 0; j < i_indexes.size(); ++j)
   {
      std::cout << "\r: " << (j + 1) << " / " << i_indexes.size() << std::flush;

      pbio::Template::Ptr first;
      pbio::Template::Ptr second;
      const std::string firstPath = i_dataset.getPath(i_indexes[j].first);
      const std::string secondPath = i_dataset.getPath(i_indexes[j].second);
      try
      {
         first = getTemplate(firstPath, *m_capturer, *m_recognizer, m_memory);
         second = getTemplate(secondPath, *m_capturer, *m_recognizer, m_memory);
      }
      catch (const pbio::Error &e)
      {
         char buf[1000];

         sprintf_s(buf, "fold: %d; paths: %s; %s; exception: %s", i_foldNum, firstPath.c_str(), secondPath.c_str(), e.what());
         m_logger.log(buf);
         continue;
      }

      LARGE_INTEGER start, end, freq;
      QueryPerformanceFrequency(&freq);
      QueryPerformanceCounter(&start);

      const float score = (-m_recognizer->verifyMatch(*first, *second).distance);
      QueryPerformanceCounter(&end);

      io_recTimes.push_back((end.QuadPart - start.QuadPart) * 1000.0 / freq.QuadPart);
      io_scores.push_back(score);
   }
   std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////////
void CTester::DoTest(const std::string& i_pairsFilepath
   , const std::string& i_lfwDir
   , const std::string& i_tarsFarsFile)
{
   // open lfw dataset
   LFW_Dataset dataset(i_pairsFilepath, i_lfwDir);

   // process images
   std::vector<float> match_scores;
   std::vector<float> mismatch_scores;

   std::vector<double> recTimes;

   for (int i = 0; i < dataset.getFoldsCount(); ++i)
   {
      std::vector<int> foldVector;
      foldVector.push_back(i);

      std::cout << "fold: " << i << std::endl;

      IndexSource* const idx_source = dataset.createIndexSource(foldVector);

      const std::vector<std::pair<int, int> > matches = idx_source->all_matches();
      const std::vector<std::pair<int, int> > mismatches = idx_source->all_mismatches();

      processOneFolder(dataset, matches, i, recTimes, match_scores);
      processOneFolder(dataset, mismatches, i, recTimes, mismatch_scores);

      delete idx_source;
   }

   double avgRecWorkTime = 0;
   for (auto x : recTimes)
   {
      avgRecWorkTime += x;
   }
   avgRecWorkTime /= recTimes.size();

   double dispRecWorkTime = 0;
   for (auto x : recTimes)
   {
      dispRecWorkTime += (x - avgRecWorkTime) * (x - avgRecWorkTime);
   }
   dispRecWorkTime /= recTimes.size();

   m_logger.log("Recognizer work time is M = " + std::to_string(avgRecWorkTime) + "; D = " + std::to_string(dispRecWorkTime));

   printScoresIntoFile(i_tarsFarsFile, match_scores, mismatch_scores);
}
