#ifndef __LFW_DATASET_H__
#define __LFW_DATASET_H__

#include <stdint.h>
#include <string>

#include "IndexSource.h"

class LFW_Dataset
{
public:

	virtual ~LFW_Dataset(){}

	LFW_Dataset(
		const std::string pairs_file,
		const std::string lfw_dir);

	virtual std::string getPath(const int i) const;

	virtual int getFoldsCount() const;

	virtual IndexSource* createIndexSource(
		std::vector<int> folds) const;

private:
	std::vector<std::string> _all_paths;

	typedef std::pair<int, int> index_pair;

	typedef RestrictedIndexSource::RestrictedFold lfw_restricted_fold;

	std::vector<lfw_restricted_fold> _restricted_folds;

	static std::string getPath(
		const std::string &lfw_dir,
		const std::string &name,
		const int n);

	static int countPersonImages(
		const std::string &name,
		const std::string &lfw_dir);
};

#endif  // __LFW_DATASET_H__
