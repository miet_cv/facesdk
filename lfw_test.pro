TEMPLATE = app
CONFIG += qt
QT += gui core testlib

unix {
    QMAKE_CXXFLAGS += -fopenmp -lgomp
    LIBS += -fopenmp -lgomp
}

include($${TOP_PROJECT_DIR}/tools/tools.pri)
include($${TOP_PROJECT_DIR}/config.pri)

INCLUDEPATH += ../../../src/libfacerec/libfacerec/include/
INCLUDEPATH += ../../../src/face_recognition/libanalysis/include/

INCLUDEPATH += $${OUTPUT_DIR}/include
INCLUDEPATH += $${OUTPUT_DIR}/include/face_recognition

LIBS += -lfacerec
LIBS += -ldl -lpthread -lrt
LIBS += -lopencv_core -lopencv_imgproc -lopencv_highgui
LIBS += -ljpeg
LIBS += -lz
LIBS += -lpng -ljpeg -ltiff

LIBS += -lrassert
LIBS += -lbinaryio
LIBS += -llowrankmetric
LIBS += -lfvf_2
LIBS += -lpreprocessors


QMAKE_CXXFLAGS += -export-dynamic

SOURCES += \
	make_megaface_landmarks.cpp \
	make_face_scrub_landmarks.cpp \
	train_lrm_over_CNN.cpp \
	ijba_test.cpp \
	show_ijba_set.cpp \
	CSVFileReader.h \
	CSVFileReader.cpp \
	test_nist_sd32_meds2.cpp \
	compare_lfw_results.cpp \
	torch_utils.cpp \
	SimpleDataset.h \
	SimpleDataset.cpp \
	convert_lrm_xml2bin.cpp \
	feret_test.cpp \
	IndexSource.h \
	IndexSource.cpp \
	prepare_datasets.cpp \
	CASIA_Dataset.h \
	CASIA_Dataset.cpp \
	linearize_roc.cpp \
	combine_rocs.cpp \
	Dataset.h \
	Dataset.cpp \
	LFW_Dataset.h \
	LFW_Dataset.cpp \
	common.h \
	common.cpp \
	train_lrm_on_lfw_casia.cpp \
	LRMTrainer.h \
	LRMTrainer.cpp \
	fvf_train.cpp \
	lfw_test.cpp \
	Tester.cpp \
	Tester.h \
	main.cpp


src_installTarget()



